const images = document.querySelectorAll('.image-to-show');
const imgarray = Array.from(images);

function showNextImage() {
  let currentImageIndex = 0;
  return function() {
    imgarray[currentImageIndex].style.display = 'none';
    currentImageIndex = (currentImageIndex + 1) % imgarray.length;
    imgarray[currentImageIndex].style.display = 'block';
  }
}

const showImage = showNextImage();

imgarray.forEach(function(img) {
  img.style.display = 'none';
});

setInterval(() => {
  showImage();
}, 3000);